package com.avelycure.gloriousgeckomobile.data

import com.avelycure.gloriousgeckomobile.core.Host
import com.avelycure.gloriousgeckomobile.core.HostActivity

class RepositoryImpl(
    private val hostsApi: HostsApi,
    private val hostsInfoApi: HostInfoApi
): Repository {
    override suspend fun getHosts(token: String): List<Host> {
        return hostsApi.getHosts(token).map { Host(it.ip, it.hostName, it.isActive) }
    }

    override suspend fun getHostInfo(host: Host, token: String): List<HostActivity> {
        return hostsInfoApi.getHostInfo(host, token).map { HostActivity(it.timestamp, it.isActive) }
    }
}