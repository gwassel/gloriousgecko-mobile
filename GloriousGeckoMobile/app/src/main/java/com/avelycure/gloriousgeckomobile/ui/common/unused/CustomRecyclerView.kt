package com.avelycure.gloriousgeckomobile.ui.common.unused

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

//when swipe to top, make bottom items move to right and left instead of bottom
class CustomRecyclerView: RecyclerView {
    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {

    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        val lastItemIndex = (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
        Log.d("mytag", "last item: $lastItemIndex")
    }
}