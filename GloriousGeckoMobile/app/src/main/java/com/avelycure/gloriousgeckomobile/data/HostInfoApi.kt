package com.avelycure.gloriousgeckomobile.data

import com.avelycure.gloriousgeckomobile.core.Host

interface HostInfoApi {
    fun getHostInfo(remoteHost: Host, token: String): List<RemoteHostActivity>
}