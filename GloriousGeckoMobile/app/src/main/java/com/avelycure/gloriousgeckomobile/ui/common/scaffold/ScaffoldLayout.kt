package com.avelycure.gloriousgeckomobile.ui.common.scaffold

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View.MeasureSpec.AT_MOST
import android.view.View.MeasureSpec.EXACTLY
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.avelycure.gloriousgeckomobile.ui.common.BottomSheet
import com.avelycure.gloriousgeckomobile.ui.common.RoundButton
import com.avelycure.gloriousgeckomobile.ui.common.unused.CustomRecyclerView
import java.lang.Float.min
import kotlin.concurrent.thread

class ScaffoldLayout : FrameLayout {
    private lateinit var paint: Paint
    private var width = 0f
    private var height = 0f
    private var backgroundColor: Int = Color.BLUE
    private var state: ScaffoldState = ScaffoldState.Closed

    private var currentBottomSheetOffset =  70F
    private var defaultBottomSheetOffset =  70F
    private var bottomSheetContentHeight = 0f

    private lateinit var bottomSheet: BottomSheet
    private lateinit var customRecyclerView: RecyclerView
    private lateinit var roundButton: RoundButton
    private var isInited = false

    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {
        paint = Paint()
        thread {
            while(true) {
                Log.d("mytag", "bottomsheet offset $defaultBottomSheetOffset")
                Thread.sleep(500)
            }
        }
    }

    fun initLayout(
        bs: BottomSheet,
        rv: RecyclerView,
        b: RoundButton
    ) {
        bottomSheet = bs
        customRecyclerView = rv
        roundButton = b

        currentBottomSheetOffset = roundButton.measuredHeight.toFloat()

        roundButton.setOnClickListener {
            toggle()
            bottomSheet.onStateChanged(state)
        }
        isInited = true
    }

    private fun updateState() {
        state = if (state is ScaffoldState.Closed) {
            ScaffoldState.Opened
        } else {
            ScaffoldState.Closed
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        if (!isInited) {
            return
        }

        val displayMetrics = Resources.getSystem().displayMetrics

        val desiredWidth = displayMetrics.widthPixels.toFloat()
        val desiredHeight = displayMetrics.heightPixels.toFloat()

        val widthSize = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val heightSize = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        children.forEach { childView ->
            if (childView !is CustomRecyclerView) {
                measureChild(childView, widthMeasureSpec, heightMeasureSpec)
            }
        }
        val button = children.find { it is RoundButton }!!

        defaultBottomSheetOffset = button.measuredHeight.toFloat()
        bottomSheetContentHeight = bottomSheet.bottomSheetContentHeight
        val layoutHeight = MeasureSpec.getSize(heightMeasureSpec)
        val heightMeasureSpec = MeasureSpec.makeMeasureSpec(layoutHeight - defaultBottomSheetOffset.toInt(), EXACTLY)
        customRecyclerView.measure(widthMeasureSpec, heightMeasureSpec)

        width = when (widthMode) {
            EXACTLY -> {
                widthSize
            }
            AT_MOST -> {
                min(desiredWidth, widthSize)
            }
            else -> {
                desiredWidth
            }
        }

        height = when (heightMode) {
            EXACTLY -> {
                heightSize
            }
            AT_MOST -> {
                min(desiredHeight, heightSize)
            }
            else -> {
                desiredHeight
            }
        }

        setMeasuredDimension(width.toInt(), height.toInt())
    }

    // прилетают сюда глобальные координаты
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
//        val viewGroupWidth = right - left
//        val viewGroupHeight = bottom - top
//        Log.d("mytag", "h $height, $measuredHeight, $viewGroupHeight")//все одинаково
//        Log.d("mytag", "w $width, $measuredWidth, $viewGroupWidth")
        if (!isInited) {
            return
        }

        children.forEach { view ->
            if (view is RecyclerView) {
                view.layout(
                    0,
                    0,
                    view.measuredWidth,
                    view.measuredHeight
                )//пока не вызовем на ребенке layout, width and height не будут в актулаьном состоянии
            }
            if (view is BottomSheet) {
                view.layout(
                    0,
                    measuredHeight - currentBottomSheetOffset.toInt(),
                    measuredWidth,
                    measuredHeight - currentBottomSheetOffset.toInt() + view.measuredHeight
                )
            }
            if (view is RoundButton) {
                view.layout(
                    measuredWidth / 2 - view.measuredWidth / 2,
                    measuredHeight - view.measuredHeight,
                    measuredWidth / 2 + view.measuredWidth / 2,
                    measuredHeight
                )
            }
        }
    }

    private fun toggle() {
        updateState()
        val offset = chooseBottomSheetOffset()
        val animator = ValueAnimator.ofFloat(currentBottomSheetOffset, offset)
        animator.duration = 500
        animator.interpolator = OvershootInterpolator(1f)
        animator.addUpdateListener { animation ->
            currentBottomSheetOffset = animation.animatedValue as Float
            requestLayout()
        }
        animator.start()
    }

    private fun chooseBottomSheetOffset(): Float {
        return if(state is ScaffoldState.Opened) {
            defaultBottomSheetOffset + bottomSheetContentHeight
        } else {
            defaultBottomSheetOffset
        }
    }
}