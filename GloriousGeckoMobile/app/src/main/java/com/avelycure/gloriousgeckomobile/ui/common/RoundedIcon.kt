package com.avelycure.gloriousgeckomobile.ui.common

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import java.lang.Float.min

class RoundedIcon : View {
    private lateinit var bitmap: Bitmap
    private lateinit var paint: Paint
    private var backgroundColor: Int = Color.WHITE
    private var path: Path = Path()
    private var pathCircle: Path = Path()
    private var width = 0f
    private var height = 0f

    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {
        bitmap = context
            .assets
            .open("placeholder.png")
            .use(BitmapFactory::decodeStream)
        paint = Paint()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val desiredWidth = 100f
        val desiredHeight = 100f

        val widthSize = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val heightSize = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)


        width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> min(desiredWidth, widthSize)
            else -> desiredWidth
        }

        height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> min(desiredHeight, heightSize)
            else -> desiredHeight
        }

        pathCircle.addCircle(width / 2, height / 2, min(width / 2, height / 2), Path.Direction.CW)
        path.addRect(0f, 0f, width, height, Path.Direction.CW)
        path.op(pathCircle, Path.Op.DIFFERENCE)

        setMeasuredDimension(width.toInt(), height.toInt())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawBitmap(bitmap, 0f, 0f, paint)

        paint.color = backgroundColor
        canvas.drawPath(path, paint)
    }
}