package com.avelycure.gloriousgeckomobile.ui.common.unused

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.view.children
import kotlin.Int


class IpLayout : FrameLayout {
    private lateinit var paint: Paint
    private var width = 0f
    private var height = 0f
    private var backgroundColor: Int = Color.BLUE

    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {
        paint = Paint()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
    }

    private fun addDigitItems() {
//        val inputItem1 = InputItem(context)
//        val inputItem2 = InputItem(context)
//        inputItem1.setText("hello123")
//        inputItem1.setText("hello123")
//        addView(inputItem1, 0, LayoutParams(100, 100))
//        addView(inputItem2, 0, LayoutParams(100, 100))
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        addDigitItems()

        val desiredWidth = 80f
        val desiredHeight = 80f

        children.forEach {
            it.measure(it.measuredWidth, measuredHeight)
        }

        val widthSize = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val heightSize = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> java.lang.Float.min(desiredWidth, widthSize)
            else -> desiredWidth
        }

        height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> java.lang.Float.min(desiredHeight, heightSize)
            else -> desiredHeight
        }

        setMeasuredDimension(width.toInt(), height.toInt())
    }
}