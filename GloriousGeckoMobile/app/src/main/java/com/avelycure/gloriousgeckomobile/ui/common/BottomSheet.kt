package com.avelycure.gloriousgeckomobile.ui.common

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.widget.LinearLayout
import androidx.core.view.children
import com.avelycure.gloriousgeckomobile.ui.common.scaffold.ScaffoldState

class BottomSheet : LinearLayout {
    private lateinit var paint: Paint
    private var width = 0f
    private var height = 0f
    private var state: State = State.Closed

    var bottomSheetContentHeight = 0f

    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {
        paint = Paint()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val displayMetrics = Resources.getSystem().displayMetrics

        var totalHeight = 0
        children.forEach {
            it.measure(widthMeasureSpec, heightMeasureSpec)
            totalHeight += it.measuredHeight
        }

        val widthSize = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val heightSize = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> widthSize
            else -> totalHeight.toFloat()
        }

        height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> heightSize
            else -> totalHeight.toFloat()
        }

        bottomSheetContentHeight = height
        height += displayMetrics.heightPixels / 2
        setMeasuredDimension(width.toInt(), height.toInt())
        background = getDrawableWithRadius()
    }

    private fun getDrawableWithRadius(): Drawable {
        val gradientDrawable = GradientDrawable()
        gradientDrawable.cornerRadii = floatArrayOf(20f, 20f, 20f, 20f, 0f, 0f, 0f, 0f)
        gradientDrawable.setColor(Color.WHITE)
        return gradientDrawable
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (state is State.Opened) {
            children.forEach { child ->
                child.layout(0, 0, child.measuredWidth, child.measuredHeight)
                child.visibility = VISIBLE
                height += child.measuredHeight
            }
        } else {
            children.forEach { child ->
                child.visibility = INVISIBLE
            }
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
        }
        return super.dispatchTouchEvent(event)
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_UP && (event.keyCode == KeyEvent.KEYCODE_DPAD_CENTER || event.keyCode == KeyEvent.KEYCODE_ENTER)) {
        }
        return super.dispatchKeyEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        super.onTouchEvent(event)
        return true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
    }

    fun onStateChanged(newState: ScaffoldState) {
        state = if (newState is ScaffoldState.Closed) {
            State.Closed
        } else {
            State.Opened
        }
    }

    sealed class State {
        object Closed : State()
        object Opened : State()
    }
}