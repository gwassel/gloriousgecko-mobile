package com.avelycure.gloriousgeckomobile.ui.common

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import androidx.appcompat.widget.AppCompatEditText
import java.lang.Integer.min

class InputItem : AppCompatEditText {
    private lateinit var paint: Paint

    private var backgroundColor: Int = Color.BLUE

    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {
        paint = Paint()
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        Log.d("mytag", "calling overriden method")
        if (getText().toString().isNotBlank()) {
            super.setText("", type)
        }
        super.setText(text, type)
    }

//
//    override fun onDraw(canvas: Canvas) {
//        super.onDraw(canvas)
//    }

//    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
//        if (event.action == MotionEvent.ACTION_UP) {
//            onClick()
//        }
//        return super.dispatchTouchEvent(event)
//    }
//
//
//    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
//        if (event.action == KeyEvent.ACTION_UP && (event.keyCode == KeyEvent.KEYCODE_DPAD_CENTER || event.keyCode == KeyEvent.KEYCODE_ENTER)) {
//            onClick()
//        }
//        return super.dispatchKeyEvent(event)
//    }
//
//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        super.onTouchEvent(event)
//        return true
//    }
//
//    private fun onClick() {
//        onToggle()
//        updateState()
//        val animator = ValueAnimator.ofFloat(angleFromVertical, state.targetAngle)
//        animator.duration = 750
//        animator.interpolator = OvershootInterpolator(5f)
//        animator.addUpdateListener { animation ->
//            angleFromVertical = animation.animatedValue as Float
//            invalidate()
//        }
//        animator.start()
//    }
}