package com.avelycure.gloriousgeckomobile.ui.main_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.avelycure.gloriousgeckomobile.R
import com.avelycure.gloriousgeckomobile.core.Host
import com.avelycure.gloriousgeckomobile.util.navigation.NavigationConstants.HOST_NAME
import com.avelycure.gloriousgeckomobile.util.navigation.NavigationConstants.IP_ADDRESS
import com.avelycure.gloriousgeckomobile.util.navigation.NavigationConstants.IS_ACTIVE

class MainAdapter(
    private val openDetails: (Bundle) -> Unit
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    private val items = mutableListOf(
        Host("123.123.123", "google.com", true),
        Host("456.456.456", "ya.ru", false),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true),
        Host("789.789.789", "vk.com", true)
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.host_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], position)
    }

    // можно сделать item expandable через custom view
    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val tvHostName: TextView = view.findViewById(R.id.item_host)
        private val tvIp: TextView = view.findViewById(R.id.item_ip)
        private val tvIsActive: TextView = view.findViewById(R.id.item_is_active)

        fun bind(item: Host, index: Int) {
            tvHostName.text = item.hostName
            tvIp.text = item.ip
            tvIsActive.text = item.isActive.toString()
            view.setOnClickListener {
                val bundle = bundleOf(
                    IP_ADDRESS to item.ip,
                    HOST_NAME to item.hostName,
                    IS_ACTIVE to item.isActive,
                )
                openDetails(bundle)
            }
        }
    }
}