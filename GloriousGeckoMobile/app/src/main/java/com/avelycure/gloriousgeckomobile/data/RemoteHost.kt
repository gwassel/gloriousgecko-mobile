package com.avelycure.gloriousgeckomobile.data

data class RemoteHost(
    val ip: String,
    val hostName: String,
    val isActive: Boolean,
)