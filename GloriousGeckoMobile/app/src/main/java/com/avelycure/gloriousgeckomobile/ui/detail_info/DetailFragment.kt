package com.avelycure.gloriousgeckomobile.ui.detail_info

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.avelycure.gloriousgeckomobile.R
import com.avelycure.gloriousgeckomobile.util.navigation.NavigationConstants.HOST_NAME
import com.avelycure.gloriousgeckomobile.util.navigation.NavigationConstants.IP_ADDRESS
import com.avelycure.gloriousgeckomobile.util.navigation.NavigationConstants.IS_ACTIVE
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private lateinit var ipAddressTextView: TextView
    private lateinit var hostNameTextView: TextView
    private lateinit var enableStatusTextView: TextView
    private lateinit var lastCheckedStateTextView: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        ipAddressTextView = view.findViewById(R.id.ip_address)
        hostNameTextView = view.findViewById(R.id.host_name)
        enableStatusTextView = view.findViewById(R.id.enable_status)
        lastCheckedStateTextView = view.findViewById(R.id.last_checked_date)
        arguments?.let {
            setViews(it)
        }
        return view
    }

    private fun setViews(arguments: Bundle) {
        ipAddressTextView.text = arguments.getString(IP_ADDRESS)
        hostNameTextView.text = arguments.getString(HOST_NAME)
        enableStatusTextView.text = arguments.getBoolean(IS_ACTIVE).toString()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}