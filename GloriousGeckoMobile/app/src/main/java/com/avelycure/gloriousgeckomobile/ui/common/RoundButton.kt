package com.avelycure.gloriousgeckomobile.ui.common

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.animation.OvershootInterpolator
import java.lang.Float.min
import kotlin.math.cos
import kotlin.math.sin

class RoundButton : View {
    private lateinit var paint: Paint

    private var backgroundColor: Int = Color.BLUE
    private var crossColor: Int = Color.WHITE

    private var width = 0f
    private var height = 0f

    private var centerX = 0f
    private var centerY = 0f
    private var radius = 0f

    private var angleFromVertical = 0f
    private var smallRadius = 0f

    private var state: State = State.Closed

    constructor(context: Context) : super(context) {
        initElements(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initElements(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initElements(attrs)
    }

    private fun initElements(attrs: AttributeSet?) {
        paint = Paint()
    }

    //all work is in pixels
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val desiredWidth = 100f
        val desiredHeight = 100f

        val widthSize = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val heightSize = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        width = when (widthMode) {
            MeasureSpec.EXACTLY -> {
                widthSize
            }

            MeasureSpec.AT_MOST -> {
                min(desiredWidth, widthSize)
            }

            else -> {
                desiredWidth
            }
        }

        height = when (heightMode) {
            MeasureSpec.EXACTLY -> {
                heightSize
            }

            MeasureSpec.AT_MOST -> {
                min(desiredHeight, heightSize)
            }

            else -> desiredHeight
        }

        centerX = width / 2
        centerY = height / 2
        radius = minOf(width, height) / 2
        smallRadius = radius * 0.8f

        setMeasuredDimension(width.toInt(), height.toInt())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.color = backgroundColor
        canvas.drawCircle(centerX, centerY, radius, paint)

        paint.color = crossColor
        paint.strokeWidth = 10f
        val angle1 = angleFromVertical.toRad()
        val angle2 = (angleFromVertical + 90).toRad()
        canvas.drawLine(
            getEnd1X(angle1),
            getEnd1Y(angle1),
            getEnd2X(angle1),
            getEnd2Y(angle1),
            paint
        )
        canvas.drawLine(
            getEnd1X(angle2),
            getEnd1Y(angle2),
            getEnd2X(angle2),
            getEnd2Y(angle2),
            paint
        )
    }

    // angle is between vertical and line, clockwise
    private fun getEnd1X(angle: Float): Float {
        val xFromCenter = sin(angle) * smallRadius
        return centerX + xFromCenter
    }

    private fun getEnd1Y(angle: Float): Float {
        val yFromCenter = cos(angle) * smallRadius
        return centerY + yFromCenter
    }

    private fun getEnd2X(angle: Float): Float {
        val xFromCenter = sin(angle) * smallRadius
        return centerX - xFromCenter
    }

    private fun getEnd2Y(angle: Float): Float {
        val yFromCenter = cos(angle) * smallRadius
        return centerY - yFromCenter
    }

    private fun Float.toRad(): Float {
        return ((this * Math.PI) / 180f).toFloat()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_UP) {
            onClick()
        }
        return super.dispatchTouchEvent(event)
    }


    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_UP && (event.keyCode == KeyEvent.KEYCODE_DPAD_CENTER || event.keyCode == KeyEvent.KEYCODE_ENTER)) {
            onClick()
        }
        return super.dispatchKeyEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        super.onTouchEvent(event)
        return true
    }

    private fun onClick() {
        updateState()
        val animator = ValueAnimator.ofFloat(angleFromVertical, state.targetAngle)
        animator.duration = 500
        animator.interpolator = OvershootInterpolator(2f)
        animator.addUpdateListener { animation ->
            angleFromVertical = animation.animatedValue as Float
            invalidate()
        }
        animator.start()
    }

    private fun updateState() {
        state = if (state is State.Closed) {
            State.Opened
        } else {
            State.Closed
        }
    }

    sealed class State {
        abstract var targetAngle: Float

        object Closed : State() {
            override var targetAngle: Float = 0f
        }

        object Opened : State() {
            override var targetAngle: Float = 45f
        }
    }
}