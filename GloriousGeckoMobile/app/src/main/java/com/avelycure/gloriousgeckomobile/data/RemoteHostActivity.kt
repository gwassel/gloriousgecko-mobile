package com.avelycure.gloriousgeckomobile.data

data class RemoteHostActivity(
    val timestamp: Long,
    val isActive: Boolean
)