package com.avelycure.gloriousgeckomobile.util.navigation

object NavigationConstants {
    const val IP_ADDRESS = "IP_ADDRESS"
    const val HOST_NAME = "HOST_NAME"
    const val IS_ACTIVE = "IS_ACTIVE"
}