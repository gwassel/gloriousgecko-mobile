package com.avelycure.gloriousgeckomobile.core

data class HostActivity(
    val timestamp: Long,
    val isActive: Boolean
)