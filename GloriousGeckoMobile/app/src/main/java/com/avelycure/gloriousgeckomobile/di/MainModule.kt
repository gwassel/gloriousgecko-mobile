package com.avelycure.gloriousgeckomobile.di

import com.avelycure.gloriousgeckomobile.data.HostInfoApi
import com.avelycure.gloriousgeckomobile.data.HostInfoApiImpl
import com.avelycure.gloriousgeckomobile.data.HostsApi
import com.avelycure.gloriousgeckomobile.data.HostsApiImpl
import com.avelycure.gloriousgeckomobile.data.Repository
import com.avelycure.gloriousgeckomobile.data.RepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MyModule {
//
//    @Provides
//    @Singleton
//    fun provideHostInfoApi(): HostInfoApi {
//        return HostInfoApiImpl()
//    }
//
//    @Provides
//    @Singleton
//    fun provideHostApi(): HostsApi {
//        return HostsApiImpl()
//    }
//
//    @Provides
//    @Singleton
//    fun provideRepository(
//        hostsApi: HostsApi,
//        hostInfoApi: HostInfoApi,
//    ): Repository {
//        return RepositoryImpl(
//            hostsApi,
//            hostInfoApi
//        )
//    }
}