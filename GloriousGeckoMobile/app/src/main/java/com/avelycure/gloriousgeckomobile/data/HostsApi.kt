package com.avelycure.gloriousgeckomobile.data

interface HostsApi {
    fun getHosts(token: String): List<RemoteHost>
}