package com.avelycure.gloriousgeckomobile.core

data class Host(
    val ip: String,
    val hostName: String,
    val isActive: Boolean
)
