package com.avelycure.gloriousgeckomobile.ui.common.scaffold

sealed class ScaffoldState {
    object Closed : ScaffoldState()

    object Opened : ScaffoldState()
}