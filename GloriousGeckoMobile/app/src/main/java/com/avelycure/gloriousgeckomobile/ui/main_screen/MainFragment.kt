package com.avelycure.gloriousgeckomobile.ui.main_screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import androidx.recyclerview.widget.RecyclerView
import com.avelycure.gloriousgeckomobile.R
import com.avelycure.gloriousgeckomobile.ui.common.BottomSheet
import com.avelycure.gloriousgeckomobile.ui.common.scaffold.ScaffoldLayout
import com.avelycure.gloriousgeckomobile.ui.common.RoundButton
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: MainAdapter
    private lateinit var mainViewModel: MainFragmentViewModel

    private lateinit var scaffoldLayout: ScaffoldLayout
    private lateinit var bottomSheet: BottomSheet
    private lateinit var roundButton: RoundButton

    private lateinit var tvIp: TextView
    private lateinit var tvHost: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        adapter = MainAdapter { bundle ->
            findNavController(this).navigate(R.id.SecondFragment, bundle)
        }

        scaffoldLayout = view.findViewById(R.id.main_screen_layout)
        recyclerView = view.findViewById(R.id.rv)
        bottomSheet = view.findViewById(R.id.bottom_sheet)
        roundButton = view.findViewById(R.id.round_button)

        tvIp = view.findViewById(R.id.tv_ip)
        tvIp.text = resources.getText(R.string.ip)
        tvHost = view.findViewById(R.id.tv_hostname)
        tvHost.text = resources.getText(R.string.hostname)

        scaffoldLayout.initLayout(bottomSheet, recyclerView, roundButton)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext(), VERTICAL, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}