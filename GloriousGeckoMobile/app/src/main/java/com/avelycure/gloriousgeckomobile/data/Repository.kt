package com.avelycure.gloriousgeckomobile.data

import com.avelycure.gloriousgeckomobile.core.Host
import com.avelycure.gloriousgeckomobile.core.HostActivity

interface Repository {
    suspend fun getHosts(token: String): List<Host>
    suspend fun getHostInfo(host: Host, token: String) : List<HostActivity>
}